/* ===
    Allow us to use environment variables
=== */
require('dotenv').config()

/* ===
    App level imports
=== */
const serverless    = require("serverless-http")
const express       = require("express")
const bodyParser    = require("body-parser")
const cors          = require("cors")
const AWS           = require('aws-sdk')
AWS.config.update({region: 'us-east-1'})

/* ===
    Middleware imports
=== */
const add_request_credentials   = require("./middleware/add_request_credentials")
const set_response_headers      = require("./middleware/set_response_headers")
const jwt_auth                  = require("./middleware/jwt_auth")


/* ===
    Route defs imports
=== */
const get_all_products              = require("./requests/get_all_products")
const update_customer_dob           = require("./requests/update_customer_dob")
const update_customer_title         = require("./requests/update_customer_title")
const get_all_articles              = require("./requests/get_all_articles")
const get_order_details             = require("./requests/get_order_details")
const get_kustomer_status           = require("./requests/get_kustomer_status")
const subscription_paused           = require("./webhooks/subscription_paused")
const get_canceled_orders           = require("./requests/get_canceled_orders")
const multipass_login               = require("./requests/multipass_login")
const get_delivery_dates            = require("./requests/get_delivery_dates")
const get_account_activation_url    = require("./requests/get_account_activation_url")
const customer_logged_in            = require("./requests/customer_logged_in")


/* ===
    App level middleware defs
=== */
const app = express()
// Body parse to use JSON in requests
app.use(bodyParser.json())
// CORS enabled
app.use(cors())
// Set CORS and headers
app.use(set_response_headers)
// Mutate our request and add credentials
app.use(add_request_credentials)
// Create JWT auth for protected routed
app.use(jwt_auth)


/* ===
    Start the app (Not required when operating in Serverless environment)
=== */
// const port = process.env.PORT || 3002
// app.listen( port, () => console.log(`App listening at port ${port}`))



/*
Request structure
    {
        any
    }
*/
app.post('/get_all_products', ( req, res ) => {
    console.log("Getting all products....")
    get_all_products(req.shopify_api_url)
    .then( json => {
        res.status(200).send(json)
    })
})



/*
Request structure
    {
        customer_id: int,
        email: string,
        dob: string
    }
*/
app.post('/update_customer_dob', ( req, res ) => {
    const { customer_id, dob, email } = req.body
    console.log(`Updating/creating customer dob metafield....${customer_id}`)
    update_customer_dob(req.shopify_api_url, req.klaviyo_public_token, customer_id, dob, email)
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Request structure
    {
        customer_id: int,
        email: string,
        title: string
    }
*/
app.post('/update_customer_title', ( req, res ) => {
    const { customer_id, title, email } = req.body
    console.log(`Updating/creating customer title metafield....${customer_id}`)
    update_customer_title(req.shopify_api_url, req.klaviyo_public_token, customer_id, title, email)
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Request structure
    {
        "blog_handle": string
    }
*/
app.post('/get_all_articles', ( req, res ) => {
    console.log("Getting all articles....")
    get_all_articles(req.shopify_api_url, req.body.blog_handle)
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Request structure
    {
        "email": string,
        "order_number": string
    }
*/
app.post('/get_order_details', ( req, res ) => {
    console.log("Getting order details and tracking....")
    get_order_details(req.shopify_api_url,req.body.email, req.body.order_number)
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Request structure
    {
        
    }
*/
app.post('/get_kustomer_status', ( req, res ) => {
    console.log("Getting status of Kustomer Chat....")
    get_kustomer_status()
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Request structure
    {
        webhook data
    }
*/
app.post('/bold_webhook/subscription_paused', ( req, res ) => {
    console.log("Responding to subscription paused webhook....")
    subscription_paused( req.body, req.klaviyo_public_token, req.my_shopify_url, req.bold_secret, req.shopify_api_url )
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Login to management (PROTECTED ROUTE)
    {
        "email": string,
        "shopify_id": string,
        "customer_signature": string
    }
*/
app.post('/bold_management/login', ( req, res ) => {
    console.log("Loging in....")
    res.status(200).send({
        "token": req.jwt
    })
})




/*
Get canceled orders (PROTECTED ROUTE)
Use JWT auth
    {
        "email": string,
        "shopify_id": string
    }
*/
app.post('/bold_management/get_canceled_orders', ( req, res ) => {
    console.log("Getting canceled orders data....")
    get_canceled_orders(req.body.email, req.body.shopify_id, req.my_shopify_url, req.bold_secret )
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Multipass login
    {
        "email": string,
        "shopify_id": string
    }
*/
app.post('/multipass_login', ( req, res ) => {
    console.log("Generating multipass login....")
    multipass_login( req.body.email, req.body.first_name, req.body.last_name, req.body.domain, req.body.return_to, req.multipass_token )
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Delivery dates for AusPost
    {
        "to_postcode": string, 
        "network_id": string //01 = express, 02 = standard
    }
*/
app.post('/get_delivery_dates', ( req, res ) => {
    console.log("getting available delivery dates....")
    const { to_postcode, network_id } = req.body
    get_delivery_dates( to_postcode, network_id )
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Create account
    {
        customer: {
            password: string,
            first_name: string,
            last_name: string,
            email: string
        }
    }
*/
app.post('/get_account_activation_url', ( req, res ) => {
    console.log("Creating Shopify account....")
    get_account_activation_url( req.shopify_api_url, req.body.customer_id )
    .then( json => {
        res.status(200).send(json)
    })
})




/*
Add logged in tag
    {
        customer_id: "string",
        customer_tags: "string"
    }
*/
app.post('/customer_logged_in', ( req, res ) => {
    console.log("Adding tag to Shopify account....")
    customer_logged_in( req.shopify_api_url, req.body.customer_id, req.body.customer_tags )
    .then( json => {
        res.status(200).send(json)
    })
})


module.exports.handler = serverless(app)