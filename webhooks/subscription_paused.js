const bold_auth = require('../common/functions')
const fetch     = require('node-fetch')
const _         = require('lodash')

module.exports = async function subscription_paused( req_body, klaviyo_public_token, my_shopify_url, bold_secret, shopify_api_url ) {

    const { token } = await bold_auth( my_shopify_url, bold_secret )

    try {

        // Attempt to get the subscription products using the subscription id
        const subscription_id = req_body.data.subscription.id
        const customer_id = req_body.data.subscription.shopify_customer_id
        const subscription = await get_subscription_products( token, my_shopify_url, customer_id, subscription_id )
        const products = subscription.data.products

        // Attempt to get shopify products
        const shopify_products = await get_all_shopify_products( shopify_api_url )

        // Map products to new array including the ciurrent price in Shopify
        const products_mapped = products.map( product => {
            const subscription_product_sku = product.sku
            const shopify_product = shopify_products.products.find( product2 => product2.variants[0].sku == subscription_product_sku )
            product["real_shopify_price"] = shopify_product.variants[0].price
            return product
        })

        const shipping_price = subscription.data.order.order_shipping_rate.price
        
        // If the coupon exists and it's valid
        let coupon_value = 0
        if ( subscription.data.order.coupon ) {
            if ( subscription.data.order.coupon.valid === true ) {
                coupon_value = subscription.data.order.coupon.discounted_amount
            }
        }

        // If the discount exists and it's valid
        let discount_value = 0
        if ( subscription.data.order.order_discount ) {
            discount_value = parseInt(subscription.data.order.order_discount.discount_amount)
        }

        await update_klaviyo( my_shopify_url, req_body.data.subscription.customer_email, klaviyo_public_token, true, products_mapped, req_body.data.subscription, shipping_price, coupon_value, discount_value )

        return {
            success: true
        }
        
    }
    catch (error) {

        console.error(error)
        return error

    }

}




// Update Klaviyo profile
const update_klaviyo = async ( shop, email, klaviyo_public_token, paused, products, order, shipping_price, coupon_value, discount_value ) => {
    let sub_1_list_price = "null"
    let sub_1_name = "null"
    let sub_1_paid_price = "null"
    let sub_1_quantity = "null"
    let sub_2_list_price = "null"
    let sub_2_name = "null"
    let sub_2_paid_price = "null"
    let sub_2_quantity = "null"
    let sub_3_list_price = "null"
    let sub_3_name = "null"
    let sub_3_paid_price = "null"
    let sub_3_quantity = "null"
    let sub_1_img = "null"
    let sub_2_img = "null"
    let sub_3_img = "null"
    let sub_total = 0
    products.forEach(product => {
        sub_total = sub_total + parseFloat(product.price)
    })
    if ( products[0] ) {
        sub_1_list_price = parseFloat(products[0].real_shopify_price)
        sub_1_name = products[0].shopify_product.title.replace(" Subscription", "")
        sub_1_paid_price = products[0].price
        sub_1_quantity = products[0].quantity
        sub_1_img = products[0].shopify_product.image.src
    }
    if ( products[1] ) {
        sub_2_list_price = parseFloat(products[1].real_shopify_price)
        sub_2_name = products[1].shopify_product.title.replace(" Subscription", "")
        sub_2_paid_price = products[1].price
        sub_2_quantity = products[1].quantity
        sub_2_img = products[1].shopify_product.image.src
    }
    if ( products[2] ) {
        sub_3_list_price = parseFloat(products[2].real_shopify_price)
        sub_3_name = products[2].shopify_product.title.replace(" Subscription", "")
        sub_3_paid_price = products[2].price
        sub_3_quantity = products[2].quantity
        sub_3_img = products[2].shopify_product.image.src
    }
    let currency_symbol = "$"
    if ( shop === "applicationuk.myshopify.com" ) currency_symbol = "£"
    if ( shop === "applicationeu.myshopify.com" ) currency_symbol = "€"
    let sub_total_undiscounted = ((sub_total + (parseFloat(shipping_price))) - coupon_value)
    let sub_total_final = parseInt((sub_total_undiscounted - (sub_total_undiscounted*(discount_value/100))).toFixed(2))
    let data = JSON.stringify({
        "token" : klaviyo_public_token,
        "event" : "Subscription paused",
        "customer_properties" : {
            "$email" : email,
            "paused-sub" : paused,
            "sub-1-list_price": sub_1_list_price,
            "sub-2-list_price": sub_2_list_price,
            "sub-3-list_price": sub_3_list_price,
            "sub-1-name": sub_1_name,
            "sub-2-name": sub_2_name,
            "sub-3-name": sub_3_name,
            "sub-1-paid_price": sub_1_paid_price,
            "sub-2-paid_price": sub_2_paid_price,
            "sub-3-paid_price": sub_3_paid_price,
            "sub-1-quantity": sub_1_quantity,
            "sub-2-quantity": sub_2_quantity,
            "sub-3-quantity": sub_3_quantity,
            "sub-1-img": sub_1_img,
            "sub-2-img": sub_2_img,
            "sub-3-img": sub_3_img,
            "sub-frequency": order.interval_number,
            "sub-shipping": parseFloat(shipping_price),
            "sub-subtotal": sub_total,
            "sub-total": sub_total_final,
            "currency-sym": currency_symbol,
            "sub-coupon": coupon_value,
            "sub-discount": discount_value
        }
    })
    

    let buff = Buffer.from(data)
    let base64data = buff.toString('base64')
    return await fetch(`https://a.klaviyo.com/api/track?data=${base64data}`)
    .then( res => res.json() )
    .then( json => json )
}




// Get subscription products
const get_subscription_products = async ( token, shop, shopify_customer_id, subscription_id ) => {
    return await fetch(`https://ro.boldapps.net/api/third_party/manage/subscription/orders/${subscription_id}/products?customer_id=${shopify_customer_id}&shop=${shop}`, {
        headers: {
            "BOLD-Authorization": `Bearer ${token}`
        }
    })
    .then( res => res.json() )
    .then( json => json )
}



// Get all shopify products
const get_all_shopify_products = async ( shopify_api_url ) => {
    return await fetch(`${shopify_api_url}/products.json?fields=title,variants`)
    .then( res => res.json() )
    .then( json => json )
}