
# AWS Lambda - Serverless

### Run a NodeJS-Express app using AWS Lambda in the Serverless framework and provision IaaS via CloudFormation on AWS.

## 1. Introduction

**1.1 Terminology**

**IaaS** - Infrastructure as a Service. We can provision cloud-based compute infrastructure using code with AWS CloudFormation. More information related to AWS CloudFormation can be found [here](https://aws.amazon.com/cloudformation/).

**Serverless Framework** - We can provision cloud infrastructure, particularly AWS Lambda functions, using Serverless. This allows us to run local AWS Lambda functions with cost emulation before we deploy to a live environment. We won't need to directly use CloudFormation templates, we can use Serverless YML files instead. More information related to Serverless can be found [here](https://www.serverless.com/).

**AWS CLI** - A unified Command Line Interface based approach to managing AWS services. We'll be using this to setup our AWS credentials for Serverless to make use of. You can find more information regarding AWS CLI [here](https://aws.amazon.com/cli/).

**IAM** - Identity and Access Management for AWS services. More info can be found [here](https://aws.amazon.com/iam/).

**NVM** - Node Version Manager, a tool to install multiple versions of NodeJS for multiple projects.  [More info about NVM here](https://github.com/nvm-sh/nvm)

## 2. Getting Started

* Clone the project

* Install NVM or ensure you use a compatible NodeJS version (at least 12.x). It would be better to match the NodeJS version that AWS Lambda uses. At the time of writing, it's NodeJS 12.x. Check your NodeJS version
```bash
$ node -v
```
* Install project dependencies using NPM
```bash
$ npm install
```
* Install the Serverless global module. Serverless will configure an AWS CloudFormation template and provision AWS services for you. This is all configured by a config YML file.
```bash
$ npm install -g serverless
```
* You will need to configure the AWS SDK to use credentials that you have generated for your user. [See this link for using the AWS SDK to set user credentials](https://www.serverless.com/framework/docs/providers/aws/guide/credentials#setup-with-the-aws-cli)
* This project will also need to interact with DynamoDB. Ensure the IAM Role created by Serverless has the DynamoDBFullAccess attached. But we will be using the AWS SDK for CRUD operations within Node. There's no need to assign DynamoDB to Lambda directly.
* You must also create a .env file with your environment variables.

## 3. Developing Locally

We can emulate AWS production costs and run our Serverless Lambda locally. If you have successfully completed the installation steps, it's as simple as running the following
```bash
$ npm run dev
```
Your requests will emulate an AWS Lambda and provide cost estimation and billing duration. To get an understanding of AWS Lambda cost calculation, [visit this link](https://aws.amazon.com/lambda/pricing/).

## 4. Deploying Changes
It's a simple as running the following command
```bash
$ npm run deploy
```

## 5. Additional Information

We could also use JavaScript Experimental Modules to allow for ES6 imports, but I decided not to use any package bundlers (Babel, Webpack etc). 

## 6. Important Notes
Once you have provisioned infrastructure using code, you must continue to manage that infrastructure with code. You cannot go directly to AWS Management Console and make changes to commissioned services! 

If you want, you can decommission the entire build using Serverless CLI tool. The command looks like this
```bash
$ cd ./my-serverless-project
$ serverless remove
```