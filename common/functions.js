const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function bold_auth( app_domain, app_secret ) {
    const app_handle = "heroku-api"
    return await fetch(`https://ro.boldapps.net/api/auth/third_party_token?shop=${app_domain}&handle=${app_handle}`, {
        method: "get",
        headers: {
            "BOLD-Authorization": app_secret
        }
    })
    .then( res => res.json() )
    .then( json => json )
}