const jwt = require("jsonwebtoken")
require('dotenv').config()

/*
    We need to login and set a JWT token for authenticated endpoints
*/
module.exports = function( req, res, next ) {

    const private_key = process.env.secret_token

    if ( req.url.includes("/bold_management/") && !req.headers["x-jwt-token"] ) { // Need to login
        const token = jwt.sign({ "customer_signature": req.body.customer_signature }, private_key, { expiresIn: '5000' })
        req.jwt = token
        next()
    }
    else if ( req.url.includes("/bold_management/") && req.headers["x-jwt-token"] ) { //Logged in, verify the token
        try {
            // Token is valid
            var decoded = jwt.verify(req.headers["x-jwt-token"], private_key)
            next()
        } 
        catch( err ) {
            // Token is not valid and may be expired
            res.status(404).send({
                "error": "Invalid token or expired",
                "valid": false
            })
        }
    }
    else {
        next()
    }

}
