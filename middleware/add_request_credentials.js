require('dotenv').config()

/*
    Middleware to add additional params to our request object based on the request domain/origin
*/
module.exports = function ( req, res, next ) {

    let shopify_api_url
    let my_shopify_url
    let klaviyo_public_token
    let klaviyo_private_token
    let bold_secret
    let multipass_token
    if ( req.headers.origin === "https://dev.applicationteeth.com" || req.query.store === 'dev' ) { 
        shopify_api_url = process.env.shopify_api_dev 
        my_shopify_url = "application-dev.myshopify.com"
        klaviyo_private_token = process.env.klaviyo_api_secret_aus
        klaviyo_public_token = process.env.klaviyo_api_public_token_aus
        bold_secret = process.env.bold_aus_secret
        multipass_token = process.env.dev_multipass
    }
    else if ( req.headers.origin === "https://applicationteeth.com" || req.query.store === 'au' ) { 
        shopify_api_url = process.env.shopify_api_aus 
        my_shopify_url = "application.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_aus
        klaviyo_private_token = process.env.klaviyo_api_secret_aus
        bold_secret = process.env.bold_aus_secret
        multipass_token = process.env.au_multipass
    }
    else if ( req.headers.origin === "https://uk.applicationteeth.com" || req.query.store === 'uk' ) { 
        shopify_api_url = process.env.shopify_api_uk 
        my_shopify_url = "applicationuk.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_uk
        klaviyo_private_token = process.env.klaviyo_api_secret_uk
        bold_secret = process.env.bold_uk_secret
        multipass_token = process.env.uk_multipass
    }
    else if ( req.headers.origin === "https://us.applicationteeth.com" || req.query.store === 'us' ) { 
        shopify_api_url = process.env.shopify_api_us 
        my_shopify_url = "applicationus.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_us
        klaviyo_private_token = process.env.klaviyo_api_secret_us
        bold_secret = process.env.bold_us_secret
        multipass_token = process.env.us_multipass
    }
    else if ( req.headers.origin === "https://eu.applicationteeth.com" || req.query.store === 'eu' ) { 
        shopify_api_url = process.env.shopify_api_eu 
        my_shopify_url = "applicationeu.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_eu
        klaviyo_private_token = process.env.klaviyo_api_secret_eu
        bold_secret = process.env.bold_eu_secret
        multipass_token = process.env.eu_multipass
    }
    else if ( req.headers.origin === "https://int.applicationteeth.com" || req.query.store === 'int' ) { 
        shopify_api_url = process.env.shopify_api_int 
        my_shopify_url = "applicationint.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_int
        klaviyo_private_token = process.env.klaviyo_api_secret_int
        bold_secret = process.env.bold_int_secret
        multipass_token = process.env.int_multipass
    }
    else if ( req.headers.origin === "https://ca.applicationteeth.com" || req.query.store === 'ca' ) { 
        shopify_api_url = process.env.shopify_api_ca 
        my_shopify_url = "applicationca.myshopify.com"
        klaviyo_public_token = process.env.klaviyo_api_public_token_ca
        klaviyo_private_token = process.env.klaviyo_api_secret_ca
        bold_secret = process.env.bold_ca_secret
        multipass_token = process.env.ca_multipass
    }
    else { 
        shopify_api_url = process.env.shopify_api_dev 
        my_shopify_url = "application-dev.myshopify.com"
        klaviyo_private_token = process.env.klaviyo_api_secret_aus
        klaviyo_public_token = process.env.klaviyo_api_public_token_aus
        bold_secret = process.env.bold_aus_secret
        multipass_token = process.env.au_multipass
    }
    req.shopify_api_url = shopify_api_url
    req.my_shopify_url = my_shopify_url
    req.klaviyo_public_token = klaviyo_public_token
    req.klaviyo_private_token = klaviyo_private_token
    req.bold_secret = bold_secret
    req.multipass_token = multipass_token
    next()

}