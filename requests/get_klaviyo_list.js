/* 
=====================================
==== DEPRECATED - UPDATE PLANNED ====
===================================== 
*/

const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function get_klaviyo_list( shopify_api_url, klaviyo_private_token, email, segment_id ) {

    try {

        const promise_data = await Promise.all([
            shopify_products(shopify_api_url),
            get_klaviyo_subscribed_list(klaviyo_private_token, segment_id, email)
        ])

        const all_products_result = promise_data[0].products
        const in_list_result = promise_data[1]

        if ( in_list_result ) {
            return {
                "in_list": true,
                "products": all_products_result
            }
        }
        else {
            return {
                "in_list": false,
                "products": []
            }
        }

    }
    catch (error) {
        console.error(error)
        return error;
    }
}


// Get all the shopify products
const shopify_products = async ( api_url ) => {
    return await fetch(`${api_url}/products.json`)
    .then( res => res.json() )
    .then( json => json )
}


// Check if email already in klaviyo list
const get_klaviyo_subscribed_list = async ( klaviyo_private_token, segment_id, email ) => {
    const { data } = await fetch(`https://a.klaviyo.com/api/v1/segment/${segment_id}/members?email=${email}&api_key=${klaviyo_private_token}`, {
    })
    .then( res => res.json() )
    .then( json => json)
    return data.length  ? true : false
}