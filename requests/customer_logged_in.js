const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function customer_logged_in( shopify_api_url, customer_id, customer_tags ) {

    try {

        const tags = [...customer_tags, "has_logged_in"].join()
        await update_customer( shopify_api_url, customer_id, tags )
        return {
            success: true
        }

    }
    catch ( error ) {
        console.error(error)
        return error
    }

}



const update_customer = async ( api_url, customer_id, customer_tags ) => {
    return await fetch(`${api_url}/customers/${customer_id}.json`, {
        method: "put",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            customer: {
                id: customer_id,
                tags: customer_tags
            }
        })
    })
    .then( res => res.json() )
    .then( json => json )
}