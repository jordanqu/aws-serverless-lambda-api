const fetch = require('node-fetch')

module.exports = async function get_all_products( api_url ) {
    try {
        const products = await shopify_products(api_url)
        return {
            success: true,
            products: products.products
        }
    }
    catch (error) {
        console.error(error)
        return error;
    }
}


const shopify_products = async ( api_url ) => {
    return await fetch(`${api_url}/products.json`)
    .then( res => res.json() )
    .then( json => json )
}