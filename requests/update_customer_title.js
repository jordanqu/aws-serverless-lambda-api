const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function update_customer_title( shopify_api_url, klaviyo_public_token, customer_id, title, customer_email ) {

    try {

        const customer_metafields = await get_customer_metafields( shopify_api_url, customer_id )
        let title_metafield = _.find(customer_metafields.metafields, { "key": "title", "namespace": "customer" })
        if ( title_metafield ) {
            await update_metafield( shopify_api_url, customer_id, title, title_metafield.id )
        }
        else {
            await create_metafield( shopify_api_url, customer_id, title )
        }
        update_klaviyo( klaviyo_public_token, customer_email, title )
        return {
            "success": true
        }

    }
    catch (error) {

        console.error(error)
        return error

    }

}



// Get customer metafields
const get_customer_metafields = async ( shopify_api_url, id ) => {
    return await fetch(`${shopify_api_url}/customers/${id}/metafields.json`)
    .then( res => res.json() )
    .then( json => json )
}


// Update customer metafield
const update_metafield = async ( shopify_api_url, customer_id, title, metafield_id ) => {
    return await fetch(`${shopify_api_url}/customers/${customer_id}/metafields/${metafield_id}.json`, {
        method: "put",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "metafield": {
                "id": metafield_id,
                "value": title,
                "value_type": "string"
            }
        })
    })
    .then( res => res.json() )
    .then( json => json )
}


// Create customer metafield
const create_metafield = async ( shopify_api_url, id, title ) => {
    return await fetch(`${shopify_api_url}/customers/${id}.json`, {
        method: "put",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "customer": {
                id,
                "metafields": [
                    {
                        "key": "title",
                        "value": title,
                        "value_type": "string",
                        "namespace": "customer"
                    }
                ]
            }
        })
    })
    .then( res => res.json() )
    .then( json => json )
}



// Update Klaviyo profile
const update_klaviyo = async ( klaviyo_public_token, email, title ) => {
    let data = JSON.stringify({
        "token" : klaviyo_public_token,
        "properties" : {
            "$email" : email,
            "$title" : title
        }
    })
    let buff = Buffer.from(data)
    let base64data = buff.toString('base64')
    return await fetch(`https://a.klaviyo.com/api/identify?data=${base64data}`)
    .then( res => res.json() )
    .then( json => json )
}