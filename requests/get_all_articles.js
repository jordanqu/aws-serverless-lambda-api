const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function get_all_articles( api_url, handle ) {
    try {
        const { blogs } = await get_blogs( api_url )
        const blog = _.find(blogs, { handle })
        if ( blog ) {
            const { articles } = await get_articles( api_url, blog.id )
            return {
                success: true,
                articles
            }
        }
        else {
            return {
                success: false,
                articles: []
            }
        }
    }
    catch (error) {
        console.error(error)
        return error;
    }
}


const get_blogs = async ( api_url ) => {
    return await fetch(`${api_url}/blogs.json`)
    .then( res => res.json() )
    .then( json => json )
}


const get_articles = async ( api_url, blog_id ) => {
    return await fetch(`${api_url}/blogs/${blog_id}/articles.json`)
    .then( res => res.json() )
    .then( json => json )
}