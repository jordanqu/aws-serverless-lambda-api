const Multipassify = require('multipassify')

module.exports = async function multipass_login( email, first_name, last_name, shop_domain, return_to, multipass_token ) {

    try {

        // Construct the Multipassify encoder
        const multipassify = new Multipassify(multipass_token);

        // Create your customer data hash
        const customer_data = { 
            email,
            first_name,
            last_name,
            return_to
        }

        // Encode a Multipass token
        const token = multipassify.encode(customer_data);

        // Generate a Shopify multipass URL to your shop
        const url = multipassify.generateUrl(customer_data, shop_domain);

        return {
            url
        }

    }
    catch (error) {

        console.error(error)
        return error

    }

}
