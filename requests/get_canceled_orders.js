const bold_auth = require('../common/functions')
const fetch     = require('node-fetch')
const _         = require('lodash')

module.exports = async function get_canceled_orders( email, shopify_id, my_shopify_url, bold_secret ) {

    const { token } = await bold_auth( my_shopify_url, bold_secret )

    try {

        const { subscriptions } = await get_subscription_orders( token, my_shopify_url, shopify_id )

        return {
            subscriptions
        }

    }
    catch ( error ) {

    }

}



// Get all inactive subscription orders
const get_subscription_orders = async ( token, shop, shopify_customer_id ) => {
    return await fetch(`https://ro.boldapps.net/api/third_party/v2/subscriptions?shop=${shop}&shopify_customer_id=${shopify_customer_id}&active=false`, {
        headers: {
            "BOLD-Authorization": `Bearer ${token}`
        }
    })
    .then( res => res.json() )
    .then( json => json )
}