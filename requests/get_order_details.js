const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function get_order_details( shopify_api_url, email, order_name ) {
    try {
        // Get the shopify customer by email
        let customer
        try {
            customer = await get_customer( shopify_api_url, email )
        }
        catch( error ) {
            return {
                'success': false,
                'order_number': order_name,
                'message': error.message,
                'fulfillments': [],
                'code': 0
            }
        }

        // Get the customer orders via customer id
        let order
        try {
            order = await get_customer_orders( shopify_api_url, customer.id, order_name )
        }
        catch( error ) {
            return {
                'success': false,
                'order_number': order_name,
                'message': error.message,
                'fulfillments': [],
                'code': error.message === 'Order Number invalid.' ? 0 : 1
            }
        }
        
        // Success response
        return {
            'success': true,
            'order_number': order_name,
            'fulfillments': order.fulfillments,
            'code': 1
        }
    }
    catch (error) {
        console.error(error)
        return error;
    }
}




// Search for customer in shopify
const get_customer = async ( api_url, email ) => {
    const { customers } = await fetch(`${api_url}/customers/search.json?query=email:${email}`)
    .then( res => res.json() )
    .then( json => json )
    if ( !customers.length ) {
        throw Error('Email address invalid.')
    }
    else {
        return customers[0]
    }
}

// Get order details
const get_customer_orders = async ( api_url, customer_id, order_name ) => {
    const { orders } = await fetch(`${api_url}/orders.json?customer_id=${customer_id}&status=any`)
    .then( res => res.json() )
    .then( json => json )
    const order = _.find(orders, { 'name': order_name })
    if ( order && order.fulfillments.length ) {
        return order
    } 
    else if ( order && order.fulfillments.length === 0 ) {
        throw Error("Awaiting shipping information. Please allow 1-3 business days.")
    }
    else {
        throw Error("Order Number invalid.")
    }
}