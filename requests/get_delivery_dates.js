const moment    = require('moment')
const parser    = require('fast-xml-parser')
const fetch     = require('node-fetch')
const _         = require('lodash')

module.exports = async function get_delivery_dates( to_postcode, network_id ) {

    try {

        const dates = await delivery_dates( to_postcode, network_id )

        return {
            dates: dates["DeliveryEstimateRequestResponse"]["DeliveryEstimateDates"]["DeliveryEstimateDate"]
        }


    }
    catch (error) {

        console.error(error)
        return error

    }

}

// Get available dates from AusPost API according to service id
const delivery_dates = ( to_postcode, network_id ) => {
    const date_today = moment().tz('Australia/Brisbane').format('YYYY-MM-DD')
    return fetch(`https://api.auspost.com.au/DeliveryDates.xml?toPostcode=${to_postcode}&fromPostcode=4215&lodgementDate=${date_today}&networkId=${network_id}`, {
        headers: {
            'Authorization': 'Basic ' + Buffer.from(`7a0d9cd3-69df-442e-aac3-3acb702610a8@auspost.com.au:Water321#`).toString('base64')
        }
    })
    .then( res => res.text())
    .then( xml => parser.parse(xml))
}