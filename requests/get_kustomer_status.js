const fetch = require('node-fetch')
const _     = require('lodash')
require('dotenv').config()

module.exports = async function get_kustomer_status() {
    try {
        const kustomer = await kustomer_status()
        return {
            success: true,
            kustomer
        }
    }
    catch (error) {
        console.error(error)
        return error;
    }
}


const kustomer_status = async () => {
    return await fetch("https://api.kustomerapp.com/v1/chat/settings", {
        headers: {
            Authorization: `Bearer ${process.env.kustomer_secret_token}`
        }
    })
    .then( res => res.json() )
    .then( json => json )
}