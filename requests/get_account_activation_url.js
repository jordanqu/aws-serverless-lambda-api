const fetch = require('node-fetch')
const _     = require('lodash')

module.exports = async function get_account_activation_url( api_url, customer_id ) {
    try {

        // Attempt the create account invite url generation
        let account;
        try {
            account = await activation_url( api_url, customer_id )
        }
        catch ( error ) {
            return {
                success: false,
                url: "",
            }
        }
        
        return {
            success: true,
            url: account.account_activation_url
        }
        
    }
    catch ( error ) {
        console.error(error)
        return error
    }
}




const activation_url = async ( api_url, customer_id ) => {
    return await fetch(`${api_url}/customers/${customer_id}/account_activation_url.json`, {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
        })
    })
    .then( res => res.json() )
    .then( json => json )
}